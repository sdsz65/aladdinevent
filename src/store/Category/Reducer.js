import { 
    CATEGORY_SELECT_CARD,
    CATEGORY_CREATE_AGRICULTURALMACHINERY,
    CATEGORY_REMOVE_AGRICULTURALMACHINERY,
    CATEGORY_CREATE_POULTRY_AND_LIVESTOCK,
    CATEGORY_REMOVE_POULTRY_AND_LIVESTOCK,
    CATEGORY_CREATE_IRRIGATION_SYSTEM,
    CATEGORY_REMOVE_IRRIGATION_SYSTEM
} from './ActionType';

const initialState = {
    selectedCard: 'material',
    agriculturalMachineryOptions: [
        'Ploughs', 'Harrows', 'Cultivators', 'Weeders','Hoeing machines'
    ],
    livestockOptions: [
        'Milking machines', 'Incubators or brooders for poultry', 
        'Livestock identification equipment'
    ],
    irrigationSystemOptions: [
        'Irrigation trickles', 'Irrigation overheads', 'Irrigation parts and accessories',
        'Plastic film for agriculture'
    ]
};

export default function CategoryReducer(state = initialState, action) {
    switch (action.type) {
        case CATEGORY_SELECT_CARD:
            return {
                ...state,
                selectedCard: action.payload
            };
        case CATEGORY_CREATE_AGRICULTURALMACHINERY:
            return {
                ...state,
                agriculturalMachineryOptions: 
                    [...state.agriculturalMachineryOptions, action.payload]
            }
        case CATEGORY_REMOVE_AGRICULTURALMACHINERY:
            return {
                ...state,
                agriculturalMachineryOptions: 
                    [...state.agriculturalMachineryOptions].splice(action.payload, 1)
            }
        case CATEGORY_CREATE_POULTRY_AND_LIVESTOCK:
            return {
                ...state,
                livestockOptions: 
                    [...state.livestockOptions, action.payload]
            }
        case CATEGORY_REMOVE_POULTRY_AND_LIVESTOCK:
            return {
                ...state,
                livestockOptions: 
                    [...state.livestockOptions].splice(action.payload, 1)
            }
        case CATEGORY_CREATE_IRRIGATION_SYSTEM:
            return {
                ...state,
                irrigationSystemOptions: 
                    [...state.irrigationSystemOptions, action.payload]
            }
        case CATEGORY_REMOVE_IRRIGATION_SYSTEM:
            return {
                ...state,
                irrigationSystemOptions: 
                    [...state.irrigationSystemOptions].splice(action.payload, 1)
            }
        default:
            return state;
    }
}