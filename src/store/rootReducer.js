import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import CategoryReducer from './Category/Reducer';

const CategoryConfig = {
    key: 'category',
    storage,
    whitelist: ['selectedCard', 'agriculturalMachineryOptions', 'livestockOptions', 'irrigationSystemOptions'],
};

export default combineReducers({
    category: persistReducer(CategoryConfig, CategoryReducer),
});
