import React from 'react';

const EventDetail = ({event}) => {
    console.log('event', event)
    return (
        <h3>
            You selected a meeting in {event.day}, {event.month} 
            at {event.time.hour} : {event.time.minute} {event.time.period.toUpperCase()}
        </h3>
    );
};

export default EventDetail;