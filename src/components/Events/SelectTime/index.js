import React from 'react';
import Calendar from 'components/UI/Calendar';

const times = [
    { 
        id: 1,
        day: 31, 
        month: 'September', 
        time: {
            hour: '08', 
            minute: '15', 
            period: 'am'
        }
    },
    { 
        id: 2,
        day: 31, 
        month: 'November', 
        time: {
            hour: '04', 
            minute: '30', 
            period: 'pm'
        }
    },
    { 
        id: 3,
        day: '01', 
        month: 'June', 
        time: {
            hour: '11', 
            minute: '00', 
            period: 'am'
        }
    }
];

const SelectTime = ({setSelectedTime}) => {
    const handleChooseItem = (id) => {
        setSelectedTime(times.find(item => item.id === id));
    };

    return (
        <>
            {
                times.map(item =>
                    <Calendar 
                        key={item.id}
                        month={item.month} 
                        day={item.day} 
                        time={item.time} 
                        id={item.id} 
                        handleAccept={handleChooseItem}
                    />
                )
            }
            <div>
                <div>
                    If the above slots not suitable for you, 
                    <a>please suggest another time.</a>
                </div>
                <div>
                    <a>Not interested?</a>
                </div>
            </div>
            <h4>Sincerely, Alaa</h4>
        </>
    );
};

export default SelectTime;
