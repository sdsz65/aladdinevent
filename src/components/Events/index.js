import React, { useState } from 'react';
import Logo from 'assets/images/Logo.png';
import TimeLineIcon from 'remixicon-react/TimeLineIcon';
import MapPinUserLineIcon from 'remixicon-react/MapPinUserLineIcon';
import MapPinLineIcon from 'remixicon-react/MapPinLineIcon';
import EventDetail from './EventDetail';
import SelectTime from './SelectTime';

const Event = () => {
    const [selectedTime, setSelectedTime] = useState(null);

    return (
        <div className='event-content'>
            <div className='logo'>
                <img src={Logo} alt="Aladdin" />
            </div>
            <div className='content'>
                <h4 className='heading-fourth'>
                    Hi Alaa,
                </h4>
                <p>
                    I would like to meet with you to discuss appointing your company as our distributor in Dubai. <br />
                    But the direct participants in technological progress, regardless of their level, must be exposed. And there is no doubt that clear signs of the victory of institutionalization are turned into.
                </p>

                <h3>
                    {
                        selectedTime ?
                        <span>
                            You selected a meeting in {selectedTime.day}, {selectedTime.month} at {selectedTime.time.hour} : {selectedTime.time.minute} {selectedTime.time.period.toUpperCase()}
                        </span> :
                        <span>Meeting details, please select a meeting date</span>
                    }
                </h3>

                <div className='address-container'>
                    <div>
                        <TimeLineIcon size={18} />
                        <span>30 min</span>
                    </div>
                    <div>
                        <MapPinUserLineIcon size={18} />
                        <b>Face to face</b>
                    </div>
                    <div>
                        <MapPinLineIcon size={18} />
                        <span>1 Sheikh Mohammed bin Rashid Blvd — Downtown Dubai</span>
                    </div>
                </div>
                {
                    !selectedTime &&
                    <SelectTime setSelectedTime={setSelectedTime} />
                }
            </div>
        </div>
    )
};

export default Event;