// import { i18n } from 'config';
import { i18n } from 'config';
import React from 'react';
import TwitterFillIcon from 'remixicon-react/TwitterFillIcon';
import LinkedinLineIcon from 'remixicon-react/LinkedinLineIcon';
import FacebookLineIcon from 'remixicon-react/FacebookLineIcon';
import InstagramLineIcon from 'remixicon-react/InstagramLineIcon';
const Footer = () => {
    return (
        <footer className="footer">
            <div>
                Meeting technology by Aladdin. Get your free account <a>now</a>.
            </div>
            <ul className='social-medias'>
                <li><TwitterFillIcon /></li>
                <li><LinkedinLineIcon /></li>
                <li><FacebookLineIcon /></li>
                <li><InstagramLineIcon /></li>
            </ul><br />
            <ul>
                <li>{i18n.EMAIL_PREFERENCES}</li>
                <li>{i18n.UNSUBSCRIBE}</li>
                <li>{i18n.CONTACT_US}</li>
            </ul>
        </footer>
    );
};

export default Footer;