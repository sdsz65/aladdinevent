import React from 'react';
import { Row, Col, Button } from 'antd';
import { i18n } from 'config';

const Calendar = ({month, day, time, id, handleAccept}) => {
    return (
        <div className='calendar'>
            <Row>
                <Col xs={24} sm={18} md={12} lg={10}>
                    <div className='day'>{day}</div>
                    <div className='month'>{month}</div>
                    <div className='time'>
                        <span>{time.hour}</span> :
                        <span>{time.minute}</span>
                        <span>{time.period.toUpperCase()}</span>
                    </div>
                    <div className='zone'>GMT +04:00</div>
                    <div className='button'>
                        <Button onClick={() => handleAccept(id)}>{i18n.ACCEPT}</Button>
                    </div>
                </Col>
                <Col xs={24} sm={6} md={12} lg={14}></Col>
            </Row>
        </div>
    );
};

export default Calendar;