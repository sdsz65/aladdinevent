import React from 'react';
import Breadcrumb from 'components/UI/Breadcrumb';
import { ROUTE_DASHBOARD } from 'routes';
import { i18n } from 'config';

const OperationBreadcrumb = ({title}) => {
    return (
        <Breadcrumb 
            config={[
                {link: ROUTE_DASHBOARD, title: i18n.DASHBOARD},
                {title: i18n.OPERATION},
                {title: title}
            ]}
        />
    )
};

export default OperationBreadcrumb;