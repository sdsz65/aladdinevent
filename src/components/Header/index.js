import React from 'react';
import Logo from 'assets/images/Logo.png';
import Search from 'components/UI/Search';
import UserSection from './UserSection';

const Header = () => {
    return (
        <header className='header'>
            <img src={Logo} alt="Aladdin" />
            <Search />
            <UserSection />
        </header>
    );
};

export default Header;