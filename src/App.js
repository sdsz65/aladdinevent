import React from "react";
import './assets/sass/main.scss';
import Event from 'components/Events';
import Footer from 'components/Footer';

function App() {
  return (
    <div className="App">
      <Event />
      <Footer />
    </div>
  );
}

export default App;
